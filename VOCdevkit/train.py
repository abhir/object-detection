# for data types
import numpy as np
# for visualizations
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch

# for getcwd
import os

# for VOC interface
from oct2py import Oct2Py

from skimage.segmentation import felzenszwalb
from skimage.feature import hog
from sklearn import svm
from sklearn.externals import joblib

# for image ops
from skimage.io import imread
from skimage.util import img_as_float
from skimage.transform import resize
from skimage import color

import nms

opts = {
        # segmentation: preprocessing smoothness factor
        'seg_threshold_sigma': 0.8,

        # segmentation: mininmum component size (enforced by post-processing)
        'seg_min_size' : 50,

        # segmentation: list of component sizes to be used during segmentation
        'seg_components' : [50, 120, 250, 400, 550, 700, 850],

        # segmentation: area-based rejection threshold for bounding-boxes (BBs)
        'seg_rejection_threshold' : 50,

        # hog: number of orientation bins
        'hog_orientations' : 8,

        # hog: number of pixels in each cell (X,Y)
        'hog_pixels_per_cell' : (6,6),

        # hog: number of cells per block (
        'hog_cells_per_block' : (2,2),

        # criteria for labelling:
            # overlap_criteria[0] <= x <= overlap_criteria[1] => -ve
            # >= overlap_criteria[2] => +ve sample
        'overlap_criteria' : (0.0, 0.6, 0.8), # **WARNING**: a bad choice may result in zero negative examples

        # wrap size of proposal for feature extraction
        'wrap_size' : (150, 150),

        # hog : size of array returned by hog NOTE: Dependant of previous hog* parameters
        'hog_result_size' : 18432
        }

doTesting = True
PLOT_TRAIN = False
WRITE_TO_FILE = False

def area(x):
    """
    Calculate area of rectangle x = (x0, y0, x1, y1) where
        (x0, y0) is the bottom-left corner
        (x1, y1) is the top-right corner
    return 0 if x is not a valid rectangle i.e. x0 > x1 or y0 > y1
    """

    if x[0] < x[2] and x[1] < x[3]:
        return float((x[2]-x[0])*(x[3]-x[1]))
    else:
        return 0.0

# intersection area 
intersect = lambda x,y: (max(x[0], y[0]), max(x[1], y[1]), min(x[2], y[2]), min(x[3], y[3]))
# amount of overlap according to intersection area;
# x is proposal, y is ground truth
overlap_ratio = lambda x,y: area(intersect(x,y))/(area(x)+area(y)-area(intersect(x,y)))

def region_proposals (img):
    """
    Use segmentation algorithm to generate potential object region proposals
    returns list of bounded-boxes (=[x0, y0, x1, y1])
    """
    BBoxes = []
    # we vary parameter k (related to component size) to get a good recall
    for k in opts['seg_components']:
        # get the segment # for each pixel
        segments = felzenszwalb (img, scale=k, sigma=opts['seg_threshold_sigma'], min_size=opts['seg_min_size'])

        # bounding box for each segment
        bbox = np.empty((segments.max()+1, 4), dtype='int')
        count = 0

        for i in range(segments.max()+1):
            # get all pixel positions which correspond to ith segment
            ind = (segments == i).nonzero()
            # calculate the bounded-box for each segment
            bb = [ind[1].min(), ind[0].min(), ind[1].max(), ind[0].max()]
            if area(bb) < opts['seg_rejection_threshold']:
                # reject if the BB is too small
                continue
            bbox[count] = np.array(bb, dtype='int')
            count += 1
    
        BBoxes.append(bbox[:count])

    return np.concatenate(BBoxes)

def assign_labels (calc_bbox, actual_bbox, overlap_criteria):
    """
    Assign positive/negative label according to overlap with actual(ground truth) BBs
    """

    labels = np.zeros(calc_bbox.shape[0], dtype='int')
    for gt in actual_bbox:
        found_overlapping = False
        max_overlap = 0.0
        max_indx = 0
        for i, bbox in enumerate(calc_bbox):
            overlap = overlap_ratio(bbox, gt)
            if overlap >= max_overlap:
                max_overlap = overlap
                max_indx = i

            # positive case
            if overlap >= overlap_criteria[2]:
                labels[i] = 1
                found_overlapping = True
            elif labels[i] != 1 and overlap >= overlap_criteria[0] and overlap <= overlap_criteria[1]:
                labels[i] = -1
        if not found_overlapping:
            print '\tPositive sample with', max_overlap, ' overlap',
            if max_overlap >= 0.4:
                labels[max_indx] = 1
                print 'selected'
            else:
                print 'not selected'

    validInd = np.nonzero(labels != 0)[0]
    labels = labels[validInd] == 1

    return calc_bbox[validInd], labels
   
    # check the overlap and assign label
    #ret = np.array([overlap_ratio(bbox, gt) >= overlap_criteria[2] for bbox in calc_bbox for gt in actual_bbox if overlap_ratio(bbox, gt) >= overlap_criteria[2] or (overlap_ratio(bbox, gt) >= overlap_criteria[0] and overlap_ratio(bbox,gt) <= overlap_criteria[1])])

def extractfd (img):
    """
    Extract features from image
    returns HOG featues
    """
    return hog(color.rgb2gray(img), orientations=opts['hog_orientations'], pixels_per_cell=opts['hog_pixels_per_cell'], cells_per_block=opts['hog_cells_per_block'])

def plot_BBs(img, BBs, labels):
    plt.figure()
    plt.imshow(img)
    ax = plt.gca()
    for indx in range(len(BBs)):
        i,v = BBs[indx], labels[indx]
        if v == True:
            ax.add_patch(mpatch.Rectangle((i[0],i[1]),i[2]-i[0],i[3]-i[1], facecolor="none", edgecolor="green", linewidth=5))
        else:
            ax.add_patch(mpatch.Rectangle((i[0],i[1]),i[2]-i[0],i[3]-i[1], facecolor="none", edgecolor="red", linewidth=1))
    plt.draw()
    plt.show()

  
########################################
# MAIN #
########################################

# ready octave interface
oc = Oct2Py()

# set the VOC path
VOCpath = os.getcwd() + '/VOCcode'

# initialize the VOC options
oc.addpath(VOCpath)
VOCopts = oc.VOCinit()

cls = VOCopts.classes[14]    # person
ids = oc.textread(VOCopts.imgsetpath % 'train', '%s')

#all_features = np.empty((0,opts['hog_result_size']))
all_features = []
all_labels = []
all_BBox = []

# @profile
def train(low=0, high=len(ids)):
    print cls

    global all_features
    global all_labels
    global all_BBox

    for i in range(low, high):
        print 'Extracting features for #', i

        ################### TODO: VOC handling can be simplified

        # read annotation
        rec = oc.PASreadrecord(VOCopts.annopath % ids[i])

        # find objects of class and extract difficult flags for these objects
        clsBB = []
        diff = np.array([], dtype=bool)
        if type(rec.objects['class']) is list:
            for ind,val in enumerate(rec.objects['class']):
                if val == cls:
                    clsBB.append(rec.objects.bbox[4*ind:4*ind+4])
                    diff = np.append(diff, (rec.objects.difficult[ind] == 1))
            clsBB = np.array(clsBB, dtype='int')
        elif rec.objects['class'] == cls:
            clsBB = np.array(rec.objects.bbox, dtype='int')
            diff = np.array([rec.objects.difficult == 1])

        # assign ground truth class to image
        if len(clsBB) == 0:
            gt = -1;    # no object of class
        elif any(~diff):
            gt = 1;     # at least one non-difficult object
        else:
            gt = 0      # only difficult objects

        ################### End of VOC stuff
        # any non-difficult object
        # NOTE: we're leaving images with difficult objects
        if gt > 0:
            try:
                npzfile = np.load(VOCopts.exfdpath % (ids[i]+'_'+cls) + '.npz')
                print '\tUsing cached features...'
                features = npzfile['features']
                labels = npzfile['labels']
                BBox = npzfile['BBox']
            except:
                I = img_as_float (imread (VOCopts.imgpath % ids[i]))

                # get the region proposals (bounding boxes) using a segmentation algorithm
                proposalBB = region_proposals (I)

                # criteria for labelling:
                # >= overlap_criteria[0] => -ve / +ve sample
                # >= overlap_criteria[1] => +ve sample
                overlap_criteria = opts['overlap_criteria']

                # label the proposals as +ve and -ve according to their overlap with the ground truth BBs
                proposalBB, labels = assign_labels (proposalBB, clsBB[~diff], overlap_criteria)

                if PLOT_TRAIN:
                    plot_BBs(I, proposalBB, labels)
                else:
                    print '\t', len(proposalBB), 'bounding boxes'

                # resize/wrap the proposals to same size
                wrap_size = opts['wrap_size']

                features = np.empty((proposalBB.shape[0],opts['hog_result_size']))

                # compute features for each proposal
                for ind, BB in enumerate(proposalBB):
                    wrapped_proposal = resize (I[BB[1]:BB[3],BB[0]:BB[2],:], wrap_size)

                    # get the feature descriptors (FD) and bounding boxes (BBlabel)
                    features[ind] = extractfd (wrapped_proposal)

                BBox = proposalBB
                np.savez(VOCopts.exfdpath % (ids[i]+'_'+cls), features=features, BBox=BBox, labels=labels) 


            all_features.append(features)
            all_labels.append(labels)
            all_BBox.append(BBox)

def test():
    global linSVM

    test_ids = oc.textread(VOCopts.imgsetpath % 'train', '%s')
    #test_ids = oc.textread(VOCopts.imgsetpath % VOCopts.testset, '%s')
    if WRITE_TO_FILE:
        fout = open(VOCopts.detrespath % ('vibgyor', cls), 'w')

    for i, indx in enumerate(test_ids[:50]): #NOTE
        print 'Extracting features for #', i

        try:
            npzfile = np.load(VOCopts.exfdpath % (indx+'_'+cls+'_test') + '.npz')
            print '\tUsing cached features...'
            features = npzfile['features']
            if features.shape[1] != opts['hog_result_size']:
                print '\tInvalid cache. Purging...'
                raise Exception()
            BBox = npzfile['BBox']
        except:
            I = img_as_float (imread (VOCopts.imgpath % indx))

            # get the region proposals (bounding boxes) using a segmentation algorithm
            proposalBB = region_proposals (I)

            # resize/wrap the proposals to same size
            wrap_size = opts['wrap_size']

            features = np.empty((proposalBB.shape[0],opts['hog_result_size']))

            # compute features for each proposal
            for ind, BB in enumerate(proposalBB):
                wrapped_proposal = resize (I[BB[1]:BB[3],BB[0]:BB[2],:], wrap_size)

                # get the feature descriptors (FD) and bounding boxes (BBlabel)
                features[ind] = extractfd (wrapped_proposal)

            BBox = proposalBB
            np.savez(VOCopts.exfdpath % (indx+'_'+cls+'_test'), features=features, BBox=BBox)

        print 'Predicting... '

        Probs = np.zeros(features.shape[0])
        for ind, feature in enumerate(features):
            if linSVM.predict(feature):
                if WRITE_TO_FILE:
                    Probs[ind] = linSVM.predict_proba(feature)[0][1]
                else:
                    Probs[ind] = 1

        nz = np.nonzero(Probs)[0]
        Probs = Probs[nz]
        BBox = BBox[nz]
        #indProb = np.array([(ind,linSVM.predict_proba(feature)) for ind, feature in enumerate(features) if linSVM.predict(feature)])

        prunedIndx = nms.non_max_suppression(BBox, 0.8)

        BBox = BBox[prunedIndx]
        Probs = Probs[prunedIndx]

        print 'Pruned ', len(nz)-len(prunedIndx), ' out of ', len(nz)

        for i in range(len(Probs)):
                bb = BBox[i]
                prob = Probs[i]
                print 'bb', bb, 'prob', prob
                if WRITE_TO_FILE:
                    fout.write('{s} {f} {0} {1} {2} {3}\n'.format(s=indx, f=prob, *[bb[0],bb[1],bb[2],bb[3]]))
                I = img_as_float (imread (VOCopts.imgpath % indx))
                plt.imshow(I)
                ax = plt.gca()
                ax.add_patch(mpatch.Rectangle((bb[0],bb[1]),bb[2]-bb[0],bb[3]-bb[1], facecolor="none", edgecolor="green", linewidth=2))
                plt.show()

    if WRITE_TO_FILE:
        fout.close()

"""
def prepare_cache():
    test_ids = oc.textread(VOCopts.imgsetpath % VOCopts.testset, '%s')

    for i, indx in enumerate(test_ids):
        print 'Extracting features for #', i

        try:
            npzfile = np.load(VOCopts.exfdpath % (indx) + '.npz')
            print '\tUsing cached features...'
            features = npzfile['features']
            if features.shape[1] != opts['hog_result_size']:
                print '\tInvalid cache. Purging...'
                raise Exception()
            BBox = npzfile['BBox']
        except:
            I = img_as_float (imread (VOCopts.imgpath % indx))

            # get the region proposals (bounding boxes) using a segmentation algorithm
            proposalBB = region_proposals (I)
            # resize/wrap the proposals to same size
            wrap_size = opts['wrap_size']

            BBox = np.empty((proposalBB.shape[0],4), dtype='int')
            features = np.empty((proposalBB.shape[0],opts['hog_result_size']))

            # compute features for each proposal
            for ind, BBlabel in enumerate(proposalBB):
                bb = BBlabel
                wrapped_proposal = resize (I[bb[1]:bb[3],bb[0]:bb[2],:], wrap_size)

                # get the feature descriptors (FD) and bounding boxes (BBlabel)
                features[ind] = extractfd (wrapped_proposal)
                BBox[ind] = BBlabel

            np.savez(VOCopts.exfdpath % (indx), features=features, BBox=BBox)
"""

# TRAINING
try:
    linSVM = joblib.load('svm_'+cls+'.pkl')
    print 'Using cached model...'
except:
    train(0, 20)  #NOTE
    X = np.concatenate(all_features)
    all_labels = np.concatenate(all_labels)
    all_BBox = np.concatenate(all_BBox)

    print 'Training SVM... with features.shape = ', X.shape, 'and target.shape = ', all_labels.shape

    # learn using SVM
    linSVM = svm.SVC(kernel='linear', probability=True)
    linSVM.fit(X, all_labels)
    
    joblib.dump(linSVM, 'svm_'+cls+'.pkl')

    print 'DONE!!'

print 'Testing...'

if doTesting:
    test()

print 'DONE!!'

recall, prec, ap = oc.VOCevaldet(VOCopts, 'vibgyor', cls, True)
