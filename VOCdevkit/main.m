function main()
% add path to VOCdevkit code
VOCpath = [pwd '/VOCcode'];
addpath(VOCpath);

% initialize the VOC options; VOCopts in scope now
VOCopts = VOCinit();

return;

% train and test detector for each class
%N = VOCopts.nclasses;
N = 1;
for i = 1 : N
	cls = VOCopts.classes{i};
	% train detector
	detector = train(VOCopts,cls);
	% test detector
	test(VOCopts,cls,detector);
	% compute and display PR
	[recall,prec,ap] = VOCevaldet(VOCopts,'vibgyor',cls,true);	

	if i < VOCopts.nclasses
%fprintf('press any key to continue with next class...\n');
		drawnow;
%pause;
	end
end

end

% train detector
function detector = train(VOCopts, cls)

% load 'train' image set
ids = textread(sprintf(VOCopts.imgsetpath,'train'),'%s');

% extract features and bounding boxes
detector.FD = [];
detector.bbox = {};
detector.gt = [];

tic;
for i = 1:length(ids)
	% display progress
	if toc > 1
		fprintf(stderr, '%s: train: %d/%d\n', cls, i, length(ids));
		drawnow;
		tic;
	end

	% read annotation
	rec = PASreadrecord(sprintf(VOCopts.annopath, ids{i}));

	% find objects of class and extract difficult flags for these objects
	clsinds = strmatch(cls, {rec.objects(:).class}, 'exact');
	diff = [rec.objects(clsinds).difficult];

	% assign ground truth class to image
	if isempty(clsinds)
		gt = -1;          % no objects of class
	elseif any(~diff)
		gt = 1;           % at least one non-difficult object of class
	else
		gt = 0;           % only difficult objects
	end

	% any non-diffult object
	if gt
		% extract features for image
		try
    			% try to load features
    			load(sprintf(VOCopts.exfdpath, ids{i}), 'fd');
		catch
    			% compute and save features
    			I = imread(sprintf(VOCopts.imgpath, ids{i}));
    			fd = extractfd(VOCopts, I);
    			save(sprintf(VOCopts.exfdpath, ids{i}), 'fd');
		end

		detector.FD(1:length(fd), end+1) = fd;

		% extract bounding boxes for non-difficult objects
		detector.bbox{end+1} = cat(1, rec.objects(clsinds(~diff)).bbox)';

		% mark image as positive or negative
		detector.gt(end+1) = gt;
	end
end    

end

% run detector on test images
function test(VOCopts, cls, detector)

% load test set ('val' for development kit)
[ids, gt] = textread(sprintf(VOCopts.imgsetpath, VOCopts.testset), '%s %d');

% create results file
fout = fopen(sprintf(VOCopts.clsrespath, 'vibgyor', cls), 'w');

% classify each image
tic;
for i = 1:length(ids)
	% display progress
	if toc>1
		fprintf(stderr, '%s: test: %d/%d\n', cls, i, length(ids));
		drawnow;
		tic;
	end

	try
		% try to load features
		load(sprintf(VOCopts.exfdpath, ids{i}), 'fd');
	catch
		% compute and save features
		I = imread(sprintf(VOCopts.imgpath, ids{i}));
		fd = extractfd(VOCopts, I);
		save(sprintf(VOCopts.exfdpath, ids{i}), 'fd');
	end

	% compute confidence of positive classification and bounding boxes
	[c, BB] = detect(VOCopts, detector, fd);

	% write to results file
	for j = 1:length(c)
		fprintf(fid, '%s %f %d %d %d %d\n', ids{i}, c(j), BB(:, j));
	end
end

% close results file
fclose(fout);

end
