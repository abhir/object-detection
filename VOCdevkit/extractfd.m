function fd = extractfd(VOCopts, I)
% Returns the feature descriptors for image 'I'

% slic params
regionSize = 80;
regularizer = 1;

superpixels = vl_slic(single(vl_xyz2lab(vl_rgb2xyz(I))), regionSize, regularizer);

% TODO
% Similarity algo here

num_proposals = 10;
BB = zeros(num_proposals, 4)

fd = zeros([size(BB,1) 0 0]);

for i=1:size(BB,1)
	[f,d] = vl_sift(single(rgb2gray(I(BB(i,1):BB(i,3),BB(i,2):BB(i:4)))));
	fd(1,:,:) = [d' f'];
end

return fd
